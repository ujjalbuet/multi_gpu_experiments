# How to use

1. Get into the `unified` directory and run `go build`.
1. Run `./unified -benchmark=fir` to run the simulation. You can also change
   the benchmark name in the command to run another benchmark.
