module gitlab.com/syifan/multi_gpu_experiments

require (
	gitlab.com/akita/akita v1.3.2
	gitlab.com/akita/gcn3 v1.4.6
	gitlab.com/akita/mem v1.2.5
	gitlab.com/akita/noc v1.1.3
	gitlab.com/akita/util v0.1.7
	gitlab.com/ujjalbuet/gcn3_exp v0.0.0-20190827132837-f8ea2b22ad11
)

//replace gitlab.com/akita/gcn3 => /Users/Ujjal/TSM/gcn3
replace gitlab.com/akita/gcn3 => /Users/Ujjal/TSM/Kernel/local_gcn3/gcn3
