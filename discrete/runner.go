package main

import (
	"flag"
	"fmt"
	"sync"
	"net"
	"net/http"
	_ "net/http/pprof"


	"gitlab.com/akita/akita"
	"gitlab.com/akita/gcn3"
	"gitlab.com/akita/gcn3/benchmarks"
	"gitlab.com/akita/gcn3/driver"
	"gitlab.com/akita/gcn3/platform"
	"gitlab.com/akita/util/tracing"

)



var numGPUs = flag.Int("num-gpus", 1, "The number of GPUs to use")

type cacheLatencyTracer struct {
	tracer *tracing.AverageTimeTracer
	cache  akita.Component
}


type Runner struct {
	Engine            akita.Engine
	GPUDriver         *driver.Driver
	KernelTimeCounter       *tracing.BusyTimeTracer
	PerGPUKernelTimeCounter []*tracing.BusyTimeTracer
	CacheLatencyTracers     []cacheLatencyTracer
	Benchmarks        []benchmarks.Benchmark
}

func (r *Runner) Init() {
	r.buildPlatform()
	//r.KernelTimeCounter = tracing.KernelTimeCounter()
	//r.GPUDriver.AcceptHook(r.KernelTimeCounter)
	go r.startProfilingServer()
	r.addKernelTimeTracer()
	r.addCacheLatencyTracer()
	r.GPUDriver.Run()
}


func (r *Runner) buildPlatform() {
	r.Engine, r.GPUDriver = platform.BuildNR9NanoPlatform(4)
}

func (r *Runner) startProfilingServer() {
	listener, err := net.Listen("tcp", ":0")
	if err != nil {
		panic(err)
	}

	fmt.Println("Profiling server running on:", listener.Addr().(*net.TCPAddr).Port)

	panic(http.Serve(listener, nil))
}




func (r *Runner) addKernelTimeTracer() {
	r.KernelTimeCounter = tracing.NewBusyTimeTracer(
		func(task tracing.Task) bool {
			return task.What == "*driver.LaunchKernelCommand"
		})
	tracing.CollectTrace(r.GPUDriver, r.KernelTimeCounter)

	for _, gpu := range r.GPUDriver.GPUs {
		gpuKernelTimeCountner := tracing.NewBusyTimeTracer(
			func(task tracing.Task) bool {
				return task.What == "Launch Kernel"
			})
		r.PerGPUKernelTimeCounter = append(
			r.PerGPUKernelTimeCounter, gpuKernelTimeCountner)
		tracing.CollectTrace(
			gpu.CommandProcessor.Component().(*gcn3.CommandProcessor),
			gpuKernelTimeCountner)
	}
}

func (r *Runner) addCacheLatencyTracer() {

	for _, gpu := range r.GPUDriver.GPUs {
		for _, cache := range gpu.L2Caches {
			tracer := tracing.NewAverageTimeTracer(
				func(task tracing.Task) bool {
					return task.Kind == "req_in"
				})
			r.CacheLatencyTracers = append(r.CacheLatencyTracers,
				cacheLatencyTracer{tracer: tracer, cache: cache})
			tracing.CollectTrace(cache, tracer)
		}
	}
}


func (r *Runner) AddBenchmark(b benchmarks.Benchmark) {
	gpus := make([]int, *numGPUs)
	for i := 0; i <*numGPUs; i++ {
		gpus[i] = i + 1
	}
	b.SelectGPU(gpus)
	r.Benchmarks = append(r.Benchmarks, b)
}

func (r *Runner) Run() {
	var wg sync.WaitGroup
	for _, b := range r.Benchmarks {
		wg.Add(1)
		go func(b benchmarks.Benchmark, wg *sync.WaitGroup) {
			b.Run()
			b.Verify()
			wg.Done()
		}(b, &wg)
	}
	wg.Wait()

	r.Engine.Finished()

	fmt.Printf("Kernel time: %.12f\n", r.KernelTimeCounter.BusyTime())
	fmt.Printf("Total time: %.12f\n", r.Engine.CurrentTime())
	for i, c := range r.PerGPUKernelTimeCounter {
		fmt.Printf("GPU %d kernel time: %.12f\n", i+1, c.BusyTime())
	}

	for _, tracer := range r.CacheLatencyTracers {
		fmt.Printf("Cache %s average latency %.12f\n",
			tracer.cache.Name(),
			tracer.tracer.AverageTime(),
		)
	}

}

