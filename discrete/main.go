package main

import (
	"flag"
//	"gitlab.com/ujjalbuet/gcn3_exp/exp4/clmw"
)
func main() {
	flag.Parse()

	runner := Runner{}
	runner.Init()
	benchmark := GetWorkload(runner.GPUDriver)
//	ben := clmw.NewBenchmark(runner.GPUDriver)
//	ben.Width = 262144 //32768
	runner.AddBenchmark(benchmark)

	runner.Run()
}
