package main

import (
	"flag"

	"gitlab.com/syifan/multi_gpu_experiments/workloads"
)

func main() {
	flag.Parse()

	runner := Runner{}
	runner.Init()

	b := workloads.GetWorkload(runner.GPUDriver)

	runner.AddBenchmark(b)

	runner.Run()
}
